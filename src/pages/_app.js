import * as React from 'react'
import NextApp from 'next/app'
import { CacheProvider } from '@emotion/core'
import { ThemeProvider } from 'emotion-theming'

// Use only { cache } from 'emotion'. Don't use { css }.
import { cache } from 'emotion'

import theme from '../theme'
import GlobalStyles from '../components/global/GlobalStyles'

export default class App extends NextApp {
  render() {
    const { Component, pageProps } = this.props
    return (
      <CacheProvider value={cache}>
        <ThemeProvider theme={theme}>
          {GlobalStyles}
          <Component {...pageProps} />
        </ThemeProvider>
      </CacheProvider>
    )
  }
}
