import { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// Services API
import matrixAPI from '../services/matrixAPI'
import usersAPI from '../services/usersAPI'
import chartAPI from '../services/chartAPI'

// Import utils
import getMatrix from '../utils/getMatrix'
import salesDataAdapter from '../utils/salesDataAdapter'
import revenueDataAdapter from '../utils/revenueDataAdapter'
import statisticsDataAdapter from '../utils/statisticsDataAdapter'

// Import components
import DashboardWrapper from '../components/layouts/DashboardWrapper'
import DashboardContainer from '../components/layouts/DashboardContainer'
import Header from '../components/organisms/Header'
import Sidebar from '../components/organisms/Sidebar'
import PageHeader from '../components/organisms/PageHeader'

import MatrixCardLayout from '../components/layouts/MatrixCardLayout'
import DataTableLayout from '../components/layouts/DataTableLayout'
import ChartLayout from '../components/layouts/ChartLayout'

import MartrixCard from '../components/molecules/MartrixCard'
import ChartCard from '../components/molecules/ChartCard'
import DataTable from '../components/organisms/DataTable'

import DoughnutChart from '../components/molecules/DoughnutChart'
import BarChart from '../components/molecules/BarChart'
import LineChart from '../components/molecules/LineChart'

const HomePage = ({
  matrix,
  matrixStatus,
  users,
  usersStatus,
  charts,
  chartsStatus,
}) => {
  // State for sidebar expand/shrink action
  const [isSidebarExpanded, setSidebarExpended] = useState(true)
  const [chartData, setChartData] = useState(charts)

  // handle sidebar expend/contract
  const toggleSidebar = () => {
    setSidebarExpended(isSidebarExpanded => !isSidebarExpanded)
  }

  // Charts data
  const doughnutChartDataset = salesDataAdapter(chartData)
  const lineChartDataset = revenueDataAdapter(chartData)
  const barChartDataset = statisticsDataAdapter(chartData)

  // ? Refresh chart data
  const handleChartRefresh = async () => {
    try {
      toast.info('Updating Charts!')
      const { data, status } = await chartAPI()

      if (status === 200) {
        setChartData(data)
      }
    } catch (error) {
      console.log('Something went wrong!', { error })
    }
  }

  return (
    <DashboardWrapper>
      <Header
        toggleSidebar={toggleSidebar}
        isSidebarExpanded={isSidebarExpanded}
      />
      <Sidebar isSidebarExpanded={isSidebarExpanded} />
      <DashboardContainer isSidebarExpanded={isSidebarExpanded}>
        {/* Display toast message */}
        <ToastContainer
          autoClose={2500}
          hideProgressBar={true}
          position="bottom-left"
        />

        <PageHeader />
        {/* Matrix Card Layout */}
        <MatrixCardLayout>
          {typeof matrix === 'object' && Object.keys(matrix).length
            ? Object.keys(matrix).map((matrixKey, i) => {
                const { title, illustration } = getMatrix(matrixKey)
                const { new: newData, old } = matrix[matrixKey]

                return (
                  <MartrixCard
                    key={i}
                    title={title}
                    magnitude={{ old, new: newData }}
                    illustrationUrl={illustration}
                    handleChartRefresh={handleChartRefresh}
                  />
                )
              })
            : null}
        </MatrixCardLayout>
        {/* Chart Layout */}
        <ChartLayout>
          {/* Pie Chart */}
          <ChartCard title="Daily Sales">
            <DoughnutChart dataset={doughnutChartDataset} />
          </ChartCard>
          <ChartCard title="Statistics">
            <BarChart dataset={barChartDataset} />
          </ChartCard>
          <ChartCard title="Total Revenue">
            <LineChart datasets={lineChartDataset} />
          </ChartCard>
        </ChartLayout>

        {/* Data Table Layout */}
        <DataTableLayout>
          <DataTable users={users} title="Recent Users" />
          <DataTable users={users} title="Current Users" />
        </DataTableLayout>
      </DashboardContainer>
    </DashboardWrapper>
  )
}

// Get initial props for server-side rendering
HomePage.getInitialProps = async () => {
  try {
    // ? Non blocking API calls
    const asyncUsersFetch = usersAPI()
    const asyncChartsFetch = chartAPI()
    const asyncMatrixFetch = matrixAPI()

    const { data: users, status: usersStatus } = await asyncUsersFetch
    const { data: charts, status: chartsStatus } = await asyncChartsFetch
    const { data: matrix, status: matrixStatus } = await asyncMatrixFetch

    return {
      matrix,
      matrixStatus,
      users,
      usersStatus,
      charts,
      chartsStatus,
    }
  } catch (error) {
    const { response } = error
    const { status, data } = response || {}
    console.log('Something went wrong!', { status, data })
    return {
      matrix: null,
      matrixStatus: null,
      users: null,
      usersStatus: null,
      charts: null,
      chartsStatus: null,
    }
  }
}

export default HomePage
