export default (sortBy, orderBy, filterBy, users) => {
  if (Array.isArray(users)) {
    const order = orderBy === 'ASC' ? -1 : 1

    const sortedUsers = users.sort((a, b) => {
      let nameA = a[sortBy].toLowerCase() // ignore upper and lowercase
      let nameB = b[sortBy].toLowerCase() // ignore upper and lowercase
      if (nameA < nameB) {
        return 1 * order
      }
      if (nameA > nameB) {
        return -1 * order
      }

      // names must be equal
      return 0
    })

    if (!filterBy || typeof filterBy === '') return sortedUsers
    else {
      filterBy = filterBy.toLowerCase()
      return sortedUsers.filter(user => {
        const { name, phone, location, date } = user

        if (
          name.toLowerCase().includes(filterBy) ||
          phone.toLowerCase().includes(filterBy) ||
          location.toLowerCase().includes(filterBy) ||
          date.toLowerCase().includes(filterBy)
        )
          return true
        else false
      })
    }
  }
}
