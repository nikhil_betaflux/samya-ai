// Function to generate random number
function randomNumber(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export default data => {
  if (data && data.revenue && Array.isArray(data.revenue)) {
    return data.revenue.reduce(
      (accu, dataset) => {
        const { year, magnitude } = dataset
        accu[0].values.push({ year: year, magnitude: magnitude })
        accu[1].values.push({
          year: year,
          magnitude: magnitude + randomNumber(-25000, 25000),
        })
        return accu
      },
      [
        {
          name: 'Series A',
          values: [],
        },
        {
          name: 'Series B',
          values: [],
        },
      ]
    )
  } else return []
}
