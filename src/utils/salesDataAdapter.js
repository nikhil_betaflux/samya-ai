export default data => {
  if (data && data.sales && Array.isArray(data.sales)) {
    return data.sales.map(dataset => {
      return { name: dataset.title, value: dataset.magnitude }
    })
  } else return []
}
