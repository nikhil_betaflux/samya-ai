import { format, addDays } from 'date-fns'

export default data => {
  if (data && data.stats && Array.isArray(data.stats)) {
    return data.stats.map((data, i) => {
      const { date, magnitude } = data
      return { date: format(addDays(new Date(), -i), 'yyyy-MM-dd'), magnitude }
    })
  } else return []
}
