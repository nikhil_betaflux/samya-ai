// Matrix title list
const matrixData = {
  stats: {
    title: 'Statistics',
    illustration: '/assets/icons/equalizer-24px.svg',
  },
  daily_users: {
    title: 'User Today',
    illustration: '/assets/icons/supervisor_account-24px.svg',
  },
  monthly_users: {
    title: 'Monthly Users',
    illustration: '/assets/icons/layers-24px.svg',
  },
  requests_per_minute: {
    title: 'Request Per Minute',
    illustration: '/assets/icons/av_timer-24px.svg',
  },
  total_users: {
    title: 'Total Users',
    illustration: '/assets/icons/group-24px.svg',
  },
  new_downloads: {
    title: 'New Downloads',
    illustration: '/assets/icons/get_app-24px.svg',
  },
}

export default key => {
  if (key in matrixData) return matrixData[key]
  else return { title: null, illustration: null }
}
