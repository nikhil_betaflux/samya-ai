import { useEffect } from 'react'
import styled from '@emotion/styled'
import * as d3 from 'd3'

const DoughnutChartWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

// DoughnutChart styles
const DoughnutChart = styled.div``

// DoughnutChart Element
const DoughnutChartEl = ({ dataset }) => {
  // Chart data
  const data = dataset

  let text = ''

  // Width of the chart
  let width = 200

  // Height of the chart
  let height = 200

  // Doughnut chart thickness
  let thickness = 30

  let duration = 750

  const renderDoughnutChart = () => {
    // Define the dimensions and colors
    let radius = Math.min(width, height) / 2
    let color = d3.scaleOrdinal(['#4bd396', '#3db9dc', '#f5707a', '#ff9800'])

    // ? Clean the svg if present any
    d3.select('#doughnutChart svg').remove()

    // ? Select and place SVG El inside #doughnutChart
    let pieChart = d3
      .select('#doughnutChart')
      .append('svg')
      .attr('class', 'doughnut-svg')
      .attr('width', width)
      .attr('height', height)

    d3.transition().duration(1000)

    // ? Append a group to place path inside
    let g = pieChart
      .append('g')
      .attr('transform', `translate(${width / 2}, ${height / 2})`)

    // ? Defining the arc for the doughnut chart
    let arc = d3
      .arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius)

    // ? chart data to angle calculator (starting angle, end angle etc)
    let mapDataToPie = d3
      .pie()
      .value(function (d) {
        return d.value
      })
      .sort(null)

    let path = g
      .selectAll('path')
      .data(mapDataToPie(data))
      .enter()
      .append('g')
      .on('mouseover', function (d) {
        let g = d3
          .select(this)
          .style('cursor', 'pointer')
          .style('fill', 'black')
          .append('g')
          .attr('class', 'text-group')

        g.append('text')
          .attr('class', 'name-text')
          .text(`${d.data.name}`)
          .attr('text-anchor', 'middle')
          .attr('dy', '-2.5%')
          .style('font-size', '18px')
          .style('font-weight', '600')
          .style('fill', '#575a65')

        g.append('text')
          .attr('class', 'value-text')
          .text(`${d.data.value}`)
          .attr('text-anchor', 'middle')
          .attr('dy', '7.5%')
          .style('font-size', '18px')
          .style('font-weight', '600')
          .style('fill', '#797979')
      })
      .on('mouseout', function (d) {
        d3.select(this)
          .style('cursor', 'none')
          .style('fill', color(this._current))
          .select('.text-group')
          .remove()
      })
      .append('path')
      .attr('d', arc)
      .attr('fill', (d, i) => color(i))
      .on('mouseover', function (d) {
        d3.select(this)
          .style('cursor', 'pointer')
          .style('stroke', '#fff')
          .style('stroke-width', '4px')
      })
      .on('mouseout', function (d) {
        d3.select(this)
          .style('cursor', 'none')
          .style('fill', color(this._current))
          .style('stroke', 'transparent')
          .style('stroke-width', '0')
      })
      .each(function (d, i) {
        this._current = i
      })

    g.append('text')
      .attr('text-anchor', 'middle')
      .attr('dy', '.35em')
      .text(text)
  }

  useEffect(() => {
    renderDoughnutChart()
  }, [dataset])
  return (
    <DoughnutChartWrapper>
      <DoughnutChart id="doughnutChart"></DoughnutChart>
    </DoughnutChartWrapper>
  )
}

DoughnutChartEl.defaultProps = {
  dataset: [
    { name: 'Segment 1', value: 12 },
    { name: 'Segment 2', value: 18 },
    { name: 'Segment 3', value: 30 },
  ],
}

export default DoughnutChartEl
