import { useEffect } from 'react'
import styled from '@emotion/styled'
import * as d3 from 'd3'

const LineChartWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

// LineChart styles
const LineChart = styled.div`
  .grid .tick line {
    stroke: #ddd;
    stroke-opacity: 0.3;
  }

  .domain {
    stroke: transparent;
  }

  .x-axis {
    font-size: 12px;
    color: #797979;
  }

  .y-axis .tick {
    text {
      font-size: 12px;
      color: #797979;
    }
  }
`

// LineChart Element
const LineChartEl = ({ dataset: chartDataset }) => {
  let dataset = chartDataset

  let width = 360
  let height = 240
  let margin = 50

  const renderBarChart = () => {
    // ? Set a color range for chart
    let colorScale = d3.scaleOrdinal(['#3db9dc', '#4bd396'])

    // ? parse date and format it using timeParse
    let parseDate = d3.utcParse('%Y-%m-%d')

    // ? formatting the datasets
    dataset.forEach(function (data) {
      data.date = parseDate(data.date)
      data.magnitude = +data.magnitude
    })

    // ? Scale function for x-axis (using extent to get [min, max] both)
    let xScale = d3
      .scaleTime()
      .domain(d3.extent(dataset, data => data.date))
      .range([margin, width - margin])

    // ? Scale function for y-axis (using extent to get [min, max] both)
    let yScale = d3
      .scaleLinear()
      .domain([0, d3.max(dataset, data => data.magnitude)])
      .range([height - margin, margin])

    // ? Clean the svg if present any
    d3.select('#barChart svg').remove()

    /* Add SVG */
    let svg = d3
      .select('#barChart')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('fill', 'none')
      .attr('class', 'chart-group-wrapper')

    svg
      .append('g')
      .attr('fill', 'none')
      .attr('class', 'bar-group-wrapper')
      .selectAll('rect')
      .data(dataset)
      .enter()
      .append('rect')
      .attr('fill', (d, i) => colorScale(d))
      .attr('width', 20)
      .attr('rx', 4)
      .attr('height', d => {
        return height - yScale(d.magnitude) - margin
      })
      .attr('x', d => {
        return xScale(d.date)
      })
      .attr('y', d => {
        return yScale(d.magnitude)
      })

    // ? Define x-axis and y-axis for the graph
    let xAxis = d3
      .axisBottom(xScale)
      .tickSize(0)
      .ticks(5)
      .tickFormat(d => d3.timeFormat('%d/%m')(d))

    let yAxis = d3
      .axisLeft(yScale)
      .ticks(6)
      .tickSize(0)
      .tickFormat(d => d3.format('.2s')(d))

    svg
      .append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(${margin - 50}, ${height - margin + 12})`)
      .attr('stroke', '#797979')
      .call(xAxis)
      .call(g => g.select('.domain').remove())

    svg
      .append('g')
      .attr('transform', `translate(${margin - 12}, 0)`)
      .attr('class', 'y-axis')
      .call(yAxis)
      .attr('stroke', '#797979')
      .call(g => g.select('.domain').remove())

    // ? Add grid to the graph
    svg
      .append('g')
      .attr('class', 'grid')
      .attr('transform', `translate(${margin}, 0)`)
      .call(
        d3
          .axisLeft()
          .scale(yScale)
          .tickSize(-width + margin * 2, 0, 0)
          .tickFormat('')
      )
  }
  useEffect(() => {
    renderBarChart()
  }, [chartDataset])
  return (
    <LineChartWrapper>
      <LineChart id="barChart"></LineChart>
    </LineChartWrapper>
  )
}

export default LineChartEl
