import styled from '@emotion/styled'

const MatrixCard = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  border: 1px solid #e7eaed;
  border-radius: 0.4rem;
  padding: 2.4rem;
  cursor: pointer;
`

const MatrixContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  position: relative;
`

const MatrixTitle = styled.p`
  font-size: 1.6rem;
  font-weight: 600;
  color: ${props => props.theme.colors.text.light};
`
const NewMagnitude = styled.div`
  display: flex;
  align-items: center;
  margin-top: 1rem;

  & > h4 {
    font-size: 3.2rem;
    color: ${props => props.theme.colors.text.darkest};
  }

  & > img {
    height: 2.2rem;
    width: auto;
    padding-left: 0.7rem;
  }
`

const OldMagnitude = styled.p`
  font-size: 1.6rem;
  font-weight: 400;
  color: ${props => props.theme.colors.text.dark};
  margin-top: 0.8rem;

  & > span {
    font-weight: 600;
  }
`

const Illustration = styled.div`
  position: absolute;
  right: -4px;
  top: 50%;

  & > img {
    display: block;
    height: 7.2rem;
    width: auto;
    margin-top: -3.6rem;
  }
`

// Dashboard wrapper
const MatrixCardEl = ({
  title,
  magnitude,
  illustrationUrl,
  handleChartRefresh,
}) => {
  const { new: newMagnitude, old: oldMagnitude } = magnitude
  const delta = newMagnitude - oldMagnitude

  return (
    <MatrixCard onClick={handleChartRefresh}>
      <MatrixContainer>
        <MatrixTitle>{title}</MatrixTitle>
        <NewMagnitude>
          <h4>{newMagnitude}</h4>
          <img
            src={
              delta > 0
                ? '/assets/icons/arrow_upward-24px.svg'
                : '/assets/icons/arrow_downward-24px.svg'
            }
            alt="delta icon"
          />
        </NewMagnitude>
        <OldMagnitude>
          <span>Last:</span> {oldMagnitude}
        </OldMagnitude>
        <Illustration>
          <img src={illustrationUrl} alt="matrix illustration" />
        </Illustration>
      </MatrixContainer>
    </MatrixCard>
  )
}

// Default Props
MatrixCardEl.defaultProps = {
  title: 'STATISTICS',
  magnitude: {
    old: 30125,
    new: 36125,
  },
  illustrationUrl: '/assets/icons/group-24px.svg',
}

export default MatrixCardEl
