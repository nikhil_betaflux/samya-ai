import styled from '@emotion/styled'

const ChartCard = styled.div`
  display: block;
  height: 100%;
  width: 100%;
  background-color: white;
  padding: 2.4rem;
  border: 1px solid #e7eaed;
  border-radius: 0.4rem;
`

const ChartContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`

/**
 * Chart header styles
 */

const ChartHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 2.4rem;
`

const ChartTitle = styled.h4`
  font-size: 1.8rem;
  font-weight: 600;
  color: ${props => props.theme.colors.text.darkest};
`

const ChartCardEl = ({ title, children }) => {
  return (
    <ChartCard>
      <ChartContainer>
        <ChartHeader>
          <ChartTitle>{title}</ChartTitle>
        </ChartHeader>
        {children}
      </ChartContainer>
    </ChartCard>
  )
}

ChartCardEl.defaultProps = {
  title: 'Chart Title',
}

export default ChartCardEl
