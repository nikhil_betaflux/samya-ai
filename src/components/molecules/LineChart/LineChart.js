import { useEffect } from 'react'
import styled from '@emotion/styled'
import * as d3 from 'd3'

const LineChartWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

// LineChart styles
const LineChart = styled.div`
  .grid .tick line {
    stroke: #ddd;
    stroke-opacity: 0.3;
  }

  .domain {
    stroke: transparent;
  }

  .x-axis {
    font-size: 12px;
    color: #797979;
  }

  .y-axis .tick {
    text {
      font-size: 12px;
      color: #797979;
    }
  }

  /* tip */
  .tip {
    width: 3rem;
    background-color: #eee;
    text-align: center;
    box-shadow: 0px 2px 10px -2px #aaa;
    border: 2px solid #444;
    transition: all 160ms cubic-bezier(0.455, 0.03, 0.515, 0.955);
  }
  .tip::after {
    content: ' ';
    position: absolute;
    bottom: -0.5rem;
    left: 50%;
    margin-left: -0.5rem;
    border-style: solid;
    border-width: 0.5rem 0.5rem 0 0.5rem;
    border-color: #444 transparent transparent transparent;
  }
`

// LineChart Element
const LineChartEl = ({ datasets: chartDataset }) => {
  let datasets = chartDataset

  let width = 360
  let height = 240
  let margin = 50
  let duration = 250

  let lineOpacity = '.8'
  let lineOpacityHover = '1'
  let otherLinesOpacityHover = '0.6'
  let lineStroke = '3px'
  let lineStrokeHover = '3px'

  let circleOpacity = '0.85'
  let circleOpacityOnLineHover = '0.25'
  let circleRadius = 4
  let circleRadiusHover = 6

  const renderLineChart = () => {
    // ? Set a color range for chart
    let colorScale = d3.scaleOrdinal(['#4bd396', '#3db9dc'])

    // ? parse date and format it using timeParse
    let parseDate = d3.timeParse('%Y')

    // ? formatting the datasets
    datasets.forEach(function (dataset) {
      dataset.values.forEach(function (data) {
        data.year = parseDate(data.year)
        data.magnitude = +data.magnitude
      })
    })

    // ? Scale function for x-axis (using extent to get [min, max] both)
    let xScale = d3
      .scaleTime()
      .domain(d3.extent(datasets[0].values, data => data.year))
      .range([margin, width - margin])

    // ? Scale function for y-axis (using extent to get [min, max] both)
    let yScale = d3
      .scaleLinear()
      .domain([
        0,
        d3.max(
          datasets.reduce((accu, dataset) => {
            return [...accu, ...dataset.values]
          }, []),
          data => data.magnitude
        ),
      ])
      .range([height - margin, margin])

    let tip = d3
      .select('.chart-container')
      .append('div')
      .attr('class', 'tip')
      .style('position', 'absolute')
      .style('z-index', '10')
      .style('visibility', 'hidden')

    // ? Clean the svg if present any
    d3.select('#lineChart svg').remove()

    /* Add SVG */
    let svg = d3
      .select('#lineChart')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('fill', 'none')
      .attr('class', 'chart-group-wrapper')
      .style('transform', 'translate(0, -10px)')

    // ? Line curve shape generator
    let generateLine = d3
      .line()
      .x(d => xScale(d.year))
      .y(d => yScale(d.magnitude))
      .curve(d3.curveCardinal.tension(0))

    svg
      .selectAll('.line-group')
      .data(datasets)
      .enter()
      .append('g')
      .attr('class', 'line-group')
      .style('stroke-width', lineStroke)
      .append('path')
      .attr('class', 'line')
      .attr('d', d => generateLine(d.values))
      .attr('stroke', (d, i) => colorScale(i))
      .attr('opacity', lineOpacity)

    // ? Add circles in the line
    svg
      .selectAll('circle-group')
      .data(datasets)
      .enter()
      .append('g')
      .style('fill', (d, i) => colorScale(i))
      .selectAll('circle')
      .data(d => d.values)
      .enter()
      .append('g')
      .attr('class', 'circle')

      .on('mouseover', function (d) {
        d3.select(this)
          .style('cursor', 'pointer')
          .append('text')
          .attr('class', 'text')
          .text(`${d.price}`)
          .attr('x', d => xScale(d.date) + 5)
          .attr('y', d => yScale(d.price) - 10)
      })
      .on('mouseout', function (d) {
        d3.select(this)
          .style('cursor', 'none')
          .transition()
          .duration(duration)
          .selectAll('.text')
          .remove()
      })
      .append('circle')
      .attr('cx', d => xScale(d.year))
      .attr('cy', d => yScale(d.magnitude))
      .attr('r', circleRadius)
      .style('opacity', circleOpacity)
      .on('mouseover', function (d) {
        d3.select(this)
          .transition()
          .duration(duration)
          .attr('r', circleRadiusHover)
      })
      .on('mouseout', function (d) {
        d3.select(this).transition().duration(duration).attr('r', circleRadius)
      })

    // ? Define x-axis and y-axis for the graph
    let xAxis = d3.axisBottom(xScale).tickSize(0).ticks(6)

    let yAxis = d3
      .axisLeft(yScale)
      .ticks(6)
      .tickSize(0)
      .tickFormat(d => d3.format('.2s')(d))

    svg
      .append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(0, ${height - margin + 12})`)
      .attr('stroke', '#797979')
      .call(xAxis)
      .call(g => g.select('.domain').remove())

    svg
      .append('g')
      .attr('x', margin)
      .attr('transform', `translate(${margin - 12}, 0)`)
      .attr('class', 'y-axis')
      .call(yAxis)
      .attr('stroke', '#797979')
      .call(g => g.select('.domain').remove())

    // ? Add grid to the graph
    svg
      .append('g')
      .attr('class', 'grid')
      .attr('transform', `translate(${margin}, 0)`)
      .call(
        d3
          .axisLeft()
          .scale(yScale)
          .tickSize(-width + margin * 2, 0, 0)
          .tickFormat('')
      )
  }
  useEffect(() => {
    renderLineChart()
  }, [chartDataset])
  return (
    <LineChartWrapper>
      <LineChart id="lineChart"></LineChart>
    </LineChartWrapper>
  )
}

export default LineChartEl
