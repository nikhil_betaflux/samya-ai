import styled from '@emotion/styled'
import { useEffect, useRef } from 'react'

const Header = styled.div`
  display: flex;
  position: fixed;
  top: 0px;
  left: 0px;
  height: 7rem;
  width: 100%;
  box-shadow: rgba(73, 80, 87, 0.1) 0px 0px 25px 0px;
  background: white;
  border: none;
  z-index: 10;

  .hidden {
    display: none;
  }
`

/**
 * Brand box styles
 */

const BrandBox = styled.div`
  display: flex;
  width: 24rem;
  height: 100%;
  background: ${props => props.theme.colors.sidebar.bg};
  justify-content: center;
  align-items: center;

  transition: all 0.2s ease-out;
  -webkit-transition: all 0.2s ease-out;

  & > img {
    display: block;
    max-height: 3rem;
    width: auto;
  }
`
/**
 * Brand container styles
 */

const HeaderContainer = styled.div`
  display: flex;
  flex-grow: 1;
  padding: 1rem 2rem;
  justify-content: space-between;
  align-items: center;
`

const NavigationSectionLeft = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`

const NavigationSectionRight = styled(NavigationSectionLeft)`
  justify-content: flex-end;
`

/**
 * Menu button styles
 */

const MenuButton = styled.button`
  display: flex;
  padding: 1rem;
  outline: none;
  border: none;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  background: #fff;

  & > img {
    display: block;
    width: 2.6rem;
    height: auto;
  }
`

/**
 * Header search bar styled
 */

const SearchBar = styled.div`
  display: flex;
  align-items: stretch;
  height: 4rem;
  width: 20rem;
  margin-left: 2rem;
  background-color: rgba(108, 117, 125, 0.09);
  border-radius: 30px;
  overflow: hidden;
`

const InputBox = styled.input`
  display: block;
  width: 16rem;
  padding: 0.8rem;
  padding-left: 2.4rem;
  color: #6c757d;
  background: transparent;
  font-size: 1.6rem;
  font-weight: 400;
  color: ${props => props.theme.colors.text.darkest};
  outline: none;
  border: none;
  background: transparent;
`

const SearchButton = styled.button`
  width: 4rem;
  font-size: 1.8rem;
  outline: none;
  border: none;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  & > img {
    height: 2rem;
    width: auto;
  }
`

// Nav action styles
const NavAction = styled.div`
  position: relative;
  display: flex;
  height: 3.4rem;
  width: 3.4rem;
  border: 2px solid #ddd;
  border-radius: 100%;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  &:not(:last-of-type) {
    margin-right: 2rem;
  }

  & > img {
    display: block;
    height: 1.8rem;
    width: auto;
  }
`

const CountBadge = styled.span`
  position: absolute;
  height: 1.8rem;
  width: 1.8rem;
  top: -0.7rem;
  right: -0.7rem;
  color: white;
  background-color: ${props =>
    props.color && props.theme.colors[props.color]
      ? props.theme.colors[props.color]
      : props.theme.colors.green};
  border-radius: 100%;
  font-size: 1.2rem;
  font-weight: 600;
  line-height: 1.8rem;
  text-align: center;
`

// User avatar styles
const UserProfile = styled.div`
  display: block;
  height: 3.6rem;
  width: 3.6rem;
  border-radius: 100%;
  overflow: hidden;
  cursor: pointer;

  & > img {
    width: 100%;
    height: 100%;
    display: block;
  }
`

// Dashboard wrapper

export default ({ toggleSidebar, isSidebarExpanded }) => {
  // Ref for sidebar
  const BrandBoxRef = useRef(null)

  // Handle sidebar expand and contract
  useEffect(() => {
    // ? If not expanded, shrink brand box, show small logo
    if (!isSidebarExpanded) {
      BrandBoxRef.current.style.width = '8rem'
      BrandBoxRef.current.querySelector('#lgLogo').classList.toggle('hidden')
      BrandBoxRef.current.querySelector('#smLogo').classList.toggle('hidden')
    }
    // ? If expanded, expand brand box, show large logo
    else {
      BrandBoxRef.current.style.width = '24rem'
      if (
        BrandBoxRef.current
          .querySelector('#lgLogo')
          .classList.contains('hidden')
      ) {
        BrandBoxRef.current.querySelector('#lgLogo').classList.toggle('hidden')
      }
      if (
        !BrandBoxRef.current
          .querySelector('#smLogo')
          .classList.contains('hidden')
      )
        BrandBoxRef.current.querySelector('#smLogo').classList.toggle('hidden')
    }
  }, [isSidebarExpanded])

  return (
    <Header>
      <BrandBox ref={BrandBoxRef}>
        <img src="/assets/images/SamyaLogo.png" alt="samya logo" id="lgLogo" />
        <img
          src="/assets/images/SamyaLogoSmall.png"
          alt="samya logo small"
          className="hidden"
          id="smLogo"
        />
      </BrandBox>
      <HeaderContainer>
        <NavigationSectionLeft>
          <MenuButton onClick={toggleSidebar}>
            <img src="/assets/icons/menu-24px.svg" alt="hamburger icon" />
          </MenuButton>
          <SearchBar>
            <InputBox placeholder="Search..." />
            <SearchButton>
              <img src="/assets/icons/search-24px.svg" alt="search icon" />
            </SearchButton>
          </SearchBar>
        </NavigationSectionLeft>
        <NavigationSectionRight>
          <NavAction>
            <img
              src="/assets/icons/notifications-24px.svg"
              alt="notification icon"
            />
            <CountBadge color="green">5</CountBadge>
          </NavAction>
          <NavAction>
            <img src="/assets/icons/mail-24px.svg" alt="message icon" />
            <CountBadge color="red">5</CountBadge>
          </NavAction>
          <NavAction>
            <img src="/assets/icons/settings-24px.svg" alt="setting icon" />
          </NavAction>
          <UserProfile>
            <img src="/assets/images/UserAvatar.jpg" alt="user avatar" />
          </UserProfile>
        </NavigationSectionRight>
      </HeaderContainer>
    </Header>
  )
}
