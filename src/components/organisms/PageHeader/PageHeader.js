import styled from '@emotion/styled'

const PageHeader = styled.div`
  display: flex;
  padding: 2.5rem 0;
  align-items: center;
  justify-content: space-between;
`

const PageHeading = styled.h4`
  font-size: 2rem;
  font-weight: 600;
  color: ${props => props.theme.colors.text.darkest};
`

const Breadcrumb = styled.ul`
  display: flex;
  align-items: center;
`

const Fragment = styled.li`
  font-size: 1.6rem;
  color: ${props => props.theme.colors.text.light};

  &:not(:last-of-type) {
    &::after {
      content: '/';
      padding-left: 0.5rem;
    }

    padding-right: 0.5rem;
    color: ${props => props.theme.colors.text.dark};
  }
`

/**
 * page header components
 */
const PageHeaderEl = ({ heading, routes }) => {
  return (
    <PageHeader>
      <PageHeading>{heading || 'Page Heading'}</PageHeading>

      {Array.isArray(routes) && routes.length ? (
        <Breadcrumb>
          {routes.map((fragment, i) => {
            return <Fragment key={i}>{fragment || 'Sub Section'}</Fragment>
          })}
        </Breadcrumb>
      ) : null}
    </PageHeader>
  )
}

// Default props
PageHeaderEl.defaultProps = {
  heading: 'Dashboard',
  routes: ['Samya', 'Admin', 'Dashboard'],
}

export default PageHeaderEl
