import styled from '@emotion/styled'
import { useState } from 'react'
import { format } from 'date-fns'

// Import utils
import sortAndFilterUsers from '../../../utils/sortAndFilterUsers'
const DataTable = styled.div`
  display: block;
  height: 100%;
  width: 100%;
  background-color: white;
  padding: 2.4rem;
  border: 1px solid #e7eaed;
  border-radius: 0.4rem;
`

const DataTableContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`

/**
 * Table header styles
 */

const TableHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 2.4rem;
`

const TableTitle = styled.h4`
  font-size: 1.8rem;
  font-weight: 600;
  color: ${props => props.theme.colors.text.darkest};
`

const TableActions = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const SearchBar = styled.div`
  display: flex;
  align-items: stretch;
  height: 4rem;
  width: 15rem;
  margin-left: 2rem;
  border-bottom: 1px solid rgba(108, 117, 125, 0.2);
  overflow: hidden;
`

const InputBox = styled.input`
  display: block;
  width: 12rem;
  padding: 0.8rem;
  color: #6c757d;
  background: transparent;
  font-size: 1.6rem;
  font-weight: 400;
  color: ${props => props.theme.colors.text.darkest};
  outline: none;
  border: none;
  background: transparent;
`

const SearchButton = styled.button`
  width: 3rem;
  font-size: 1.8rem;
  outline: none;
  border: none;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background: #fff;

  & > img {
    height: 2rem;
    width: auto;
  }
`

// Data table body

const TableContent = styled.div`
  display: block;
  width: 100%;
  overflow-x: auto;
`

const TableContentWrapper = styled.table`
  width: 100%;
  border-collapse: collapse;
`

const TableHead = styled.thead``

const TableRow = styled.tr`
  position: relative;

  & > th {
    padding: 1.2rem;
    font-size: 1.4rem;
    border-bottom: 2px solid #ddd;
    border-top: 1px solid #ddd;
    border-left: 0;
    border-right: 0;
    cursor: pointer;
  }

  & > td {
    padding: 1.2rem;
    font-size: 1.6rem;
    border-bottom: 1px solid #ddd;

    & > h5 {
      font-size: 1.4rem;
      font-weight: 600;
      color: ${props => props.theme.colors.text.dark};
    }

    & > span {
      font-size: 1.2rem;
      font-weight: 400;
    }
  }
`

const UserAvatar = styled.img`
  height: 3.5rem;
  width: 3.5rem;
  border-radius: 100%;
  overflow: hidden;
`

const TableBody = styled.tbody``

const TableFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2rem 4rem;
`

const PaginationBtn = styled.button`
  font-size: 1.4rem;
  padding: 0.4rem 0.8rem;
  border-radius: 0.6rem;
  background: #eee;
  outline: none;
  margin: 0px 1rem;
  cursor: pointer;
`

const PageInfo = styled.span`
  font-size: 1.4rem;
  font-weight: 600;
  padding-right: 1rem;
`

// Data table element
const DataTableEl = ({ title, users }) => {
  // Records per page
  const recordPerPage = 10
  // Total number of pages
  const numberOfPages = Math.ceil(users / recordPerPage)

  // format the date
  users.forEach(user => {
    user.date = format(new Date(user.date), 'yyyy-MM-dd')
  })

  const [userList, setUserList] = useState(users)
  const [activePage, setActivePage] = useState(1)
  const [totalPage, setTotalPage] = useState(numberOfPages)
  const [sortBy, setSortBy] = useState('name')
  const [orderBy, setOrderBy] = useState('ASC')
  const [filterBy, setFilterBy] = useState('')

  // Handle search input
  const handleSearchInput = e => {
    const { value } = e.target
    setFilterBy(value)
    setActivePage(1)
  }

  // ? set active filter
  const setActiveFilter = filterName => {
    if (['name', 'phone', 'location', 'date'].includes(filterName)) {
      setActivePage(1)
      if (sortBy === filterName) {
        setOrderBy(orderBy === 'ASC' ? 'DESC' : 'ASC')
      } else {
        setOrderBy('ASC')
        setSortBy(filterName)
      }
    }
  }

  // ? Get active users
  let filteredUsers = sortAndFilterUsers(sortBy, orderBy, filterBy, userList)

  // ? Get the current page count
  const pageCount = filteredUsers.length
    ? Math.ceil(filteredUsers.length / recordPerPage)
    : 1

  // ? Apply all filter and sorting
  filteredUsers = filteredUsers.filter((user, i) => {
    return activePage * recordPerPage - recordPerPage <= i &&
      i < activePage * recordPerPage
      ? true
      : false
  })

  return (
    <DataTable>
      <DataTableContainer>
        {/* Table Header */}

        <TableHeader>
          <TableTitle>{title}</TableTitle>
          <TableActions>
            <SearchBar>
              <InputBox
                placeholder="Search..."
                value={filterBy}
                onChange={handleSearchInput}
              />
              <SearchButton>
                <img src="/assets/icons/search-24px.svg" alt="search icon" />
              </SearchButton>
            </SearchBar>
          </TableActions>
        </TableHeader>
        {/* Table Content */}

        <TableContent>
          <TableContentWrapper>
            {/* Table head */}

            <TableHead>
              <TableRow>
                <th>#</th>
                <th
                  onClick={() => {
                    setActiveFilter('name')
                  }}
                >
                  {sortBy === 'name' ? (orderBy === 'ASC' ? '⭡ ' : '⭣ ') : ''}
                  User Name
                </th>
                <th
                  onClick={() => {
                    setActiveFilter('phone')
                  }}
                >
                  {sortBy === 'phone' ? (orderBy === 'ASC' ? '⭡ ' : '⭣ ') : ''}
                  Phone
                </th>
                <th
                  onClick={() => {
                    setActiveFilter('location')
                  }}
                >
                  {sortBy === 'location'
                    ? orderBy === 'ASC'
                      ? '⭡ '
                      : '⭣ '
                    : ''}
                  Location
                </th>
                <th
                  onClick={() => {
                    setActiveFilter('date')
                  }}
                >
                  {sortBy === 'date' ? (orderBy === 'ASC' ? '⭡ ' : '⭣ ') : ''}
                  Date
                </th>
              </TableRow>
            </TableHead>
            {/* Table body */}

            <TableBody>
              {Array.isArray(filteredUsers) && filteredUsers.length
                ? filteredUsers.map((user, i) => {
                    const { name, role, phone, date, location, avatar } = user

                    return (
                      <TableRow key={i}>
                        <td>
                          <UserAvatar src={`${avatar}`} alt="user avatar" />
                        </td>
                        <td>
                          <h5>{name}</h5>
                          <span>{role}</span>
                        </td>
                        <td>{phone}</td>
                        <td>{location}</td>
                        <td>{date}</td>
                      </TableRow>
                    )
                  })
                : null}
            </TableBody>
          </TableContentWrapper>
        </TableContent>

        {/* table footer */}
        <TableFooter>
          <PageInfo>Active Page - {activePage}</PageInfo>
          {[...Array(pageCount)].map((_, i) => {
            return (
              <PaginationBtn
                key={i}
                onClick={() => {
                  setActivePage(i + 1)
                }}
              >
                {i + 1}
              </PaginationBtn>
            )
          })}
        </TableFooter>
      </DataTableContainer>
    </DataTable>
  )
}

// Default props
DataTableEl.defaultProps = {
  title: 'Recent Users',
}

export default DataTableEl
