import styled from '@emotion/styled'
import { useEffect, useRef } from 'react'

/**
 * Sidebar styles
 */

const Sidebar = styled.div`
  position: fixed;
  width: 24rem;
  padding: 20px 0;
  top: 70px;
  left: 0px;
  bottom: 0;
  background: ${props => props.theme.colors.sidebar.bg};
  -webkit-transition: all 0.2s ease-out;
  transition: all 0.2s ease-out;
  -webkit-box-shadow: 0 0 25px 0 rgba(73, 80, 87, 0.1);
  box-shadow: 0 0 25px 0 rgba(73, 80, 87, 0.1);
  overflow: scroll;
  z-index: 10;

  transition: all 0.2s ease-out;
  -webkit-transition: all 0.2s ease-out;
`

const SidebarHint = styled.span`
  display: block;
  padding: 1rem 2rem;
  font-size: 1.2rem;
  color: ${props => props.theme.colors.sidebar.textFaded};
  font-weight: 600;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

/**
 * Side navigation styles
 */

const SideNavigation = styled.div`
  width: 100%;
  /* overflow: scroll; */
`

const NavigationList = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: stretch;
`

const NavigationItem = styled.li``

const NavigationLink = styled.a`
  display: flex;
  padding: 1.4rem 2rem;
  align-items: center;
  cursor: pointer;

  & > span {
    margin-left: 1.25rem;
    color: ${props => props.theme.colors.sidebar.textFaded};
    font-size: 1.6rem;
  }

  & > svg {
    height: 2.2rem;
    width: auto;
    fill: #98a6ad;
  }

  &:hover,
  &:active {
    background-color: ${props => props.theme.colors.sidebar.active};
    & > span {
      color: ${props => props.theme.colors.background};
    }

    & > svg {
      height: 2.2rem;
      width: auto;
      fill: ${props => props.theme.colors.background};
    }
  }
`

const LinkArrow = styled.span`
  display: flex;
  margin-left: auto !important;

  & > svg {
    height: 2.2rem;
    width: auto;
    fill: #98a6ad;
  }
`

/**
 * Navigation list level - 2
 */
const NavigationList2 = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  height: 0px;
  overflow: hidden;
`

const NavigationItem2 = styled.li``

const NavigationLink2 = styled.a`
  display: flex;
  padding: 1.4rem 2rem;
  padding-left: 5.5rem;
  align-items: center;
  cursor: pointer;
  color: ${props => props.theme.colors.sidebar.textFaded};
  font-size: 1.6rem;

  &:hover,
  &:active {
    background-color: ${props => props.theme.colors.sidebar.active};
  }
`

const Badge = styled.div`
  display: flex;
  margin-left: auto;
  background-color: ${props =>
    props.color && props.theme.colors[props.color]
      ? props.theme.colors[props.color]
      : props.theme.colors.green};
  height: 2rem;
  padding: 0.25rem 1rem;
  font-size: 1.2rem;
  color: white;
  font-weight: 600;
  border-radius: 2rem;
  line-height: 1.3rem;
`

export default ({ isSidebarExpanded }) => {
  // Ref for sidebar
  const SideBarRef = useRef(null)

  // Handle sidebar expand and contract
  useEffect(() => {
    // ? If not expanded, shrink sidebar, hide span, div
    if (!isSidebarExpanded) {
      SideBarRef.current.style.width = '8rem'
      SideBarRef.current.querySelectorAll('a > span, a > div').forEach(el => {
        el.style.display = 'none'
      })
    }
    // ? If expanded, expand sidebar, show span, div
    else {
      SideBarRef.current.style.width = '24rem'
      SideBarRef.current.querySelectorAll('a > span, a > div').forEach(el => {
        el.style.display = 'block'
      })
    }
  }, [isSidebarExpanded])

  return (
    <Sidebar ref={SideBarRef}>
      <SidebarHint>NAVIGATION</SidebarHint>
      <SideNavigation>
        <NavigationList>
          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z" />
              </svg>
              <span>Dashboard</span>
              <Badge color="green">3</Badge>
            </NavigationLink>
          </NavigationItem>

          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z" />
                <path d="M0 0h24v24H0z" fill="none" />
              </svg>
              <span>Calander</span>
            </NavigationLink>
          </NavigationItem>

          <NavigationItem className="navigation-item">
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M3 13h2v-2H3v2zm0 4h2v-2H3v2zm0-8h2V7H3v2zm4 4h14v-2H7v2zm0 4h14v-2H7v2zM7 7v2h14V7H7z" />
                <path d="M0 0h24v24H0z" fill="none" />
              </svg>
              <span>Tables</span>
              {/* <LinkArrow>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  height="24"
                  viewBox="0 0 24 24"
                  width="24"
                >
                  <path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z" />
                  <path d="M0 0h24v24H0V0z" fill="none" />
                </svg>
              </LinkArrow> */}
            </NavigationLink>
            <NavigationList2 className="navigation-list--level-2">
              <NavigationItem2>
                <NavigationLink2>Responsive Table</NavigationLink2>
              </NavigationItem2>
              <NavigationItem2>
                <NavigationLink2>Data Table</NavigationLink2>
              </NavigationItem2>
              <NavigationItem2>
                <NavigationLink2>Advance Table</NavigationLink2>
              </NavigationItem2>
            </NavigationList2>
          </NavigationItem>

          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z" />
              </svg>
              <span>Icons</span>
            </NavigationLink>
          </NavigationItem>
        </NavigationList>
      </SideNavigation>

      <SidebarHint>EXTRA</SidebarHint>
      <SideNavigation>
        <NavigationList>
          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4zm2.5 2.1h-15V5h15v14.1zm0-16.1h-15c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h15c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" />
                <path d="M0 0h24v24H0z" fill="none" />
              </svg>
              <span>Chart</span>
              <Badge color="blue">New</Badge>
            </NavigationLink>
          </NavigationItem>

          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M20.5 3l-.16.03L15 5.1 9 3 3.36 4.9c-.21.07-.36.25-.36.48V20.5c0 .28.22.5.5.5l.16-.03L9 18.9l6 2.1 5.64-1.9c.21-.07.36-.25.36-.48V3.5c0-.28-.22-.5-.5-.5zM15 19l-6-2.11V5l6 2.11V19z" />
                <path d="M0 0h24v24H0z" fill="none" />
              </svg>
              <span>Maps</span>
            </NavigationLink>
          </NavigationItem>

          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M3 5v6h5L7 7l4 1V3H5c-1.1 0-2 .9-2 2zm5 8H3v6c0 1.1.9 2 2 2h6v-5l-4 1 1-4zm9 4l-4-1v5h6c1.1 0 2-.9 2-2v-6h-5l1 4zm2-14h-6v5l4-1-1 4h5V5c0-1.1-.9-2-2-2z" />
              </svg>
              <span>Pages</span>
            </NavigationLink>
          </NavigationItem>

          <NavigationItem>
            <NavigationLink>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                viewBox="0 0 24 24"
                width="24"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z" />
              </svg>
              <span>Bookmarks</span>
            </NavigationLink>
          </NavigationItem>
        </NavigationList>
      </SideNavigation>
    </Sidebar>
  )
}
