import { Global, css } from '@emotion/core'
import { normalize } from 'polished'

// import assets

const GlobalStyles = (
  <Global
    styles={css`
      /* Normalizer css or css Reset */
      ${normalize()}

      /* Import offline first fonts */
      @font-face {
        font-family: 'Source Sans Pro';
        src: url('/assets/fonts/Source_Sans_Pro/SourceSansPro-Regular.ttf')
          format('truetype');
        font-weight: 400;
        font-style: normal;
        font-display: swap;
      }

      /* Semi-bold */
      @font-face {
        font-family: 'Source Sans Pro';
        src: url('/assets/fonts/Source_Sans_Pro/SourceSansPro-SemiBold.ttf')
          format('truetype');
        font-weight: 600;
        font-style: normal;
        font-display: swap;
      }

      * {
        box-sizing: border-box;
        padding: 0px;
        margin: 0px;
      }

      *,
      :after,
      :before {
        box-sizing: border-box;
      }

      html {
        line-height: 1.15;
        font-size: 62.5%;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        -ms-overflow-style: scrollbar;
        scroll-behavior: smooth;
      }

      body {
        font-family: 'Source Sans Pro', sans-serif;
        font-weight: 400;
        line-height: 1.4;
        font-size: 1.8rem;
        margin: 0;
        text-align: left;
        color: #797979;
        background-color: #fff;
        position: relative;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        font-family: inherit;
        line-height: 1.15;
        margin: 0px;
        color: inherit;
      }

      button {
        outline: none !important;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-tap-highlight-color: transparent;
      }

      ul {
        list-style: none;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      a {
        text-decoration: none;
        outline: none !important;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-tap-highlight-color: transparent;
      }
    `}
  />
)

export default GlobalStyles
