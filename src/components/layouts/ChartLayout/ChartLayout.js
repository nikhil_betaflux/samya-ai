import styled from '@emotion/styled'

const ChartLayout = styled.div`
  display: grid;
  grid-gap: 2.4rem;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: auto;
  margin-top: 2.4rem;

  @media (max-width: 1400px) {
    grid-template-columns: 1fr;
  }
`

// Dashboard wrapper
export default ({ children }) => {
  return <ChartLayout>{children}</ChartLayout>
}
