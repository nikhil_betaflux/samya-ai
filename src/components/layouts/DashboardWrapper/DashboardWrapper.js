import styled from '@emotion/styled'

const DashboardWrapper = styled.div`
  display: block;
  height: 100%;
  width: 100%;
  overflow: hidden;
`

// Dashboard wrapper
export default ({ children }) => {
  return <DashboardWrapper>{children}</DashboardWrapper>
}
