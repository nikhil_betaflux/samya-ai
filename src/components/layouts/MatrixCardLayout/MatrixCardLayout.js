import styled from '@emotion/styled'

const MatrixCardLayout = styled.div`
  display: grid;
  grid-gap: 2.4rem;
  grid-template-columns: repeat(6, 1fr);
  grid-auto-rows: auto;

  @media (max-width: 1800px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (max-width: 1199.98px) {
    grid-template-columns: repeat(auto-fill, minmax(22rem, 1fr));
  }
`

// Dashboard wrapper
export default ({ children }) => {
  return <MatrixCardLayout>{children}</MatrixCardLayout>
}
