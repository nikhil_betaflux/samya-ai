import styled from '@emotion/styled'
import { useEffect, useRef } from 'react'

const DashboardContainer = styled.div`
  display: block;
  min-height: 100vh;
  margin-left: 24rem;
  overflow: scroll;
  padding: 7rem 4rem 4rem;
  background-color: #f8f8f8;

  transition: all 0.2s ease-out;
  -webkit-transition: all 0.2s ease-out;
`

// Dashboard wrapper
export default ({ children, isSidebarExpanded }) => {
  // Ref for dashboard container
  const DashboardContainerRef = useRef(null)

  useEffect(() => {
    // ? If not expanded, set reduce margin-left
    if (!isSidebarExpanded) {
      DashboardContainerRef.current.style.marginLeft = '8rem'
    }
    // ? If expanded, set increase margin-left
    else {
      DashboardContainerRef.current.style.marginLeft = '24rem'
    }
  }, [isSidebarExpanded])

  return (
    <DashboardContainer ref={DashboardContainerRef}>
      {children}
    </DashboardContainer>
  )
}
