import styled from '@emotion/styled'

const DataTableLayout = styled.div`
  display: grid;
  grid-gap: 2.4rem;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: auto;
  margin-top: 2.4rem;

  @media (max-width: 1199.98px) {
    grid-template-columns: 1fr;
  }
`

// Dashboard wrapper
export default ({ children }) => {
  return <DataTableLayout>{children}</DataTableLayout>
}
