const theme = {
  colors: {},
  fontSizes: {},
  space: {},
  radii: {},
  fonts: {
    primary: `'Source Sans Pro', sans-serif`,
  },
  sizes: {},
  breakpoints: ['575.98px', '767.98px', '991.98px', '1199.98px'],
  mediaQueries: {
    // Extra small devices (portrait phones, less than 576px)
    sm: `@media (max-width: 575.98px)`,
    // Small devices (landscape phones, less than 768px)
    md: `@media (max-width: 767.98px)`,
    // Medium devices (tablets, less than 992px)
    lg: `@media (max-width: 991.98px)`,
    // Large devices (desktops, less than 1200px)
    xl: `@media (max-width: 1199.98px)`,
  },
}

// aliases
theme.colors.green = '#4bd396'
theme.colors.blue = '#3db9dc'
theme.colors.brown = '#8d6e63'
theme.colors.pink = '#f06292'
theme.colors.orange = '#ff9800'
theme.colors.red = '#f5707a'

// text colors tokens
theme.colors.text = {
  darkest: '#495057',
  dark: '#575a65',
  light: '#797979',
}

// icon colors tokens
theme.colors.icon = '#dee2e6'

theme.colors.background = '#f8f8f8'

theme.colors.sidebar = {
  bg: '#36404e',
  active: '#323b48',
  textFaded: '#98a6ad',
}

theme.breakpoints.sm = theme.breakpoints[0]
theme.breakpoints.md = theme.breakpoints[1]
theme.breakpoints.lg = theme.breakpoints[2]
theme.breakpoints.xl = theme.breakpoints[3]

export default theme
