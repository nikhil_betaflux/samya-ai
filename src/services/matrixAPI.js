import axios from 'axios'

// import constants
import { BASE_URL } from '../utils/constants'

export default () => {
  //   If base user not set, throw error
  if (!BASE_URL) throw new Error('Base URL not found!')

  return axios({
    method: 'get',
    baseURL: BASE_URL,
    url: `/api/matrix`,
  })
}
